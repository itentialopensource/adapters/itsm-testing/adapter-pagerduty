/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-pagerduty',
      type: 'Pagerduty',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Pagerduty = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Pagerduty Adapter Test', () => {
  describe('Pagerduty Class Tests', () => {
    const a = new Pagerduty(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-pagerduty-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-pagerduty-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#getApiReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getApiReference((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pagerduty-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('APIReference', 'getApiReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAbilities - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAbilities((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.abilities));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Abilities', 'getAbilities', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const abilitiesId = 'fakedata';
    describe('#getAbilitiesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAbilitiesId(abilitiesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pagerduty-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Abilities', 'getAbilitiesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const addOnsPostAddonsBodyParam = {
      addon: {
        type: 'full_page_addon',
        name: 'Internal Status Page',
        src: 'https://intranet.example.com/status'
      }
    };
    describe('#postAddons - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAddons(addOnsPostAddonsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.addon);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AddOns', 'postAddons', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAddons - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAddons(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.addons));
                assert.equal(25, data.response.limit);
                assert.equal(0, data.response.offset);
                assert.equal(false, data.response.more);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AddOns', 'getAddons', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const addOnsId = 'fakedata';
    const addOnsPutAddonsIdBodyParam = {
      addon: {
        type: 'full_page_addon',
        name: 'Internal Status Page',
        src: 'https://intranet.example.com/status'
      }
    };
    describe('#putAddonsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putAddonsId(addOnsPutAddonsIdBodyParam, addOnsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AddOns', 'putAddonsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAddonsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAddonsId(addOnsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.addon);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AddOns', 'getAddonsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const contextualSearchPostTagsBodyParam = {
      tag: {
        type: 'tag',
        label: 'Batman'
      }
    };
    describe('#postTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTags(contextualSearchPostTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.tag);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ContextualSearch', 'postTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const contextualSearchId = 'fakedata';
    const contextualSearchEntityType = 'fakedata';
    const contextualSearchPostEntityTypeIdChangeTagsBodyParam = {
      add: [
        {
          type: 'tag',
          label: 'Batman'
        },
        {
          type: 'tag_reference',
          id: 'P5IYCNZ'
        }
      ],
      remove: [
        {
          type: 'tag_reference',
          id: 'POE7RY8'
        },
        {
          type: 'tag_reference',
          id: 'PG68P1M'
        }
      ]
    };
    describe('#postEntityTypeIdChangeTags - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postEntityTypeIdChangeTags(contextualSearchEntityType, contextualSearchId, contextualSearchPostEntityTypeIdChangeTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pagerduty-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ContextualSearch', 'postEntityTypeIdChangeTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTags(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.tags);
                assert.equal(100, data.response.limit);
                assert.equal(0, data.response.offset);
                assert.equal(1, data.response.total);
                assert.equal(false, data.response.more);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ContextualSearch', 'getTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTagsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTagsId(contextualSearchId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.tag);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ContextualSearch', 'getTagsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTagsIdEntityType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTagsIdEntityType(contextualSearchId, contextualSearchEntityType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.users));
                assert.equal(100, data.response.limit);
                assert.equal(0, data.response.offset);
                assert.equal(2, data.response.total);
                assert.equal(false, data.response.more);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ContextualSearch', 'getTagsIdEntityType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEntityTypeIdTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEntityTypeIdTags(contextualSearchEntityType, contextualSearchId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.tags);
                assert.equal(100, data.response.limit);
                assert.equal(0, data.response.offset);
                assert.equal(1, data.response.total);
                assert.equal(false, data.response.more);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ContextualSearch', 'getEntityTypeIdTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const escalationPoliciesPostEscalationPoliciesBodyParam = {
      escalation_policy: {
        type: 'escalation_policy',
        name: 'Engineering Escalation Policy',
        escalation_rules: [
          {
            escalation_delay_in_minutes: 30,
            targets: [
              {
                id: 'PEYSGVF',
                type: 'user_reference'
              }
            ]
          }
        ],
        services: [
          {
            id: 'PIJ90N7',
            type: 'service_reference'
          }
        ],
        num_loops: 2,
        teams: [
          {
            id: 'PQ9K7I8',
            type: 'team_reference'
          }
        ],
        description: 'Here is the ep for the engineering team.'
      }
    };
    describe('#postEscalationPolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEscalationPolicies(escalationPoliciesPostEscalationPoliciesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.escalationPolicy);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EscalationPolicies', 'postEscalationPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEscalationPolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEscalationPolicies(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.escalation_policies));
                assert.equal(25, data.response.limit);
                assert.equal(0, data.response.offset);
                assert.equal(false, data.response.more);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EscalationPolicies', 'getEscalationPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const escalationPoliciesId = 'fakedata';
    const escalationPoliciesPutEscalationPoliciesIdBodyParam = {
      escalation_policy: {
        type: 'escalation_policy',
        name: 'Engineering Escalation Policy',
        escalation_rules: [
          {
            escalation_delay_in_minutes: 30,
            targets: [
              {
                id: 'PEYSGVF',
                type: 'user_reference'
              }
            ]
          }
        ],
        services: [
          {
            id: 'PIJ90N7',
            type: 'service_reference'
          }
        ],
        num_loops: 2,
        teams: [
          {
            id: 'PQ9K7I8',
            type: 'team_reference'
          }
        ],
        description: 'Here is the ep for the engineering team.'
      }
    };
    describe('#putEscalationPoliciesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putEscalationPoliciesId(escalationPoliciesPutEscalationPoliciesIdBodyParam, escalationPoliciesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EscalationPolicies', 'putEscalationPoliciesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEscalationPoliciesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEscalationPoliciesId(escalationPoliciesId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.escalationPolicy);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EscalationPolicies', 'getEscalationPoliciesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtensionSchemas - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExtensionSchemas((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.extension_schemas));
                assert.equal(25, data.response.limit);
                assert.equal(0, data.response.offset);
                assert.equal(false, data.response.more);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExtensionSchemas', 'getExtensionSchemas', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const extensionSchemasId = 'fakedata';
    describe('#getExtensionSchemasId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExtensionSchemasId(extensionSchemasId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.extension_schema);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExtensionSchemas', 'getExtensionSchemasId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const extensionsPostExtensionsBodyParam = {
      extension: {
        id: 'PPGPXHO',
        self: 'https://api.pagerduty.com/extensions/PPGPXHO',
        html_url: 'null',
        endpoint_url: 'https://example.com/recieve_a_pagerduty_webhook',
        name: 'My Webhook',
        summary: 'My Webhook',
        type: 'extension',
        extension_schema: {
          id: 'PJFWPEP',
          type: 'extension_schema_reference',
          summary: 'Generic Webhook',
          self: 'https://api.pagerduty.com/extension_schemas/PJFWPEP',
          html_url: 'null'
        },
        extension_objects: [
          {
            id: 'PIJ90N7',
            type: 'service_reference',
            summary: 'My Application Service',
            self: 'https://api.pagerduty.com/services/PIJ90N7',
            html_url: 'https://subdomain.pagerduty.com/services/PIJ90N7'
          }
        ]
      }
    };
    describe('#postExtensions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postExtensions(extensionsPostExtensionsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.extension);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extensions', 'postExtensions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtensions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExtensions(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.extensions));
                assert.equal(25, data.response.limit);
                assert.equal(0, data.response.offset);
                assert.equal(false, data.response.more);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extensions', 'getExtensions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const extensionsId = 'fakedata';
    const extensionsPutExtensionsIdBodyParam = {
      extension: {
        id: 'PPGPXHO',
        self: 'https://api.pagerduty.com/extensions/PPGPXHO',
        html_url: 'null',
        endpoint_url: 'https://example.com/recieve_a_pagerduty_webhook',
        name: 'My Webhook',
        summary: 'My Webhook',
        type: 'extension',
        extension_schema: {
          id: 'PJFWPEP',
          type: 'extension_schema_reference',
          summary: 'Generic Webhook',
          self: 'https://api.pagerduty.com/extension_schemas/PJFWPEP',
          html_url: 'null'
        },
        extension_objects: [
          {
            id: 'PIJ90N7',
            type: 'service_reference',
            summary: 'My Application Service',
            self: 'https://api.pagerduty.com/services/PIJ90N7',
            html_url: 'https://subdomain.pagerduty.com/services/PIJ90N7'
          }
        ]
      }
    };
    describe('#putExtensionsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putExtensionsId(extensionsPutExtensionsIdBodyParam, extensionsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extensions', 'putExtensionsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtensionsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExtensionsId(extensionsId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.extension);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extensions', 'getExtensionsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const incidentsPostIncidentsBodyParam = {
      incident: {}
    };
    describe('#postIncidents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postIncidents(incidentsPostIncidentsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.incident);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Incidents', 'postIncidents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const incidentsId = 'fakedata';
    const incidentsPostIncidentsIdNotesBodyParam = {
      note: {
        content: 'Firefighters are on the scene.'
      }
    };
    describe('#postIncidentsIdNotes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postIncidentsIdNotes(incidentsId, incidentsPostIncidentsIdNotesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.note);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Incidents', 'postIncidentsIdNotes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const incidentsPostIncidentsIdResponderRequestsBodyParam = {
      requester_id: 'string',
      message: 'string',
      responder_request_targets: [
        {}
      ]
    };
    describe('#postIncidentsIdResponderRequests - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postIncidentsIdResponderRequests(incidentsId, incidentsPostIncidentsIdResponderRequestsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.responder_request);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Incidents', 'postIncidentsIdResponderRequests', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const incidentsPostIncidentsIdSnoozeBodyParam = {
      duration: 2
    };
    describe('#postIncidentsIdSnooze - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postIncidentsIdSnooze(incidentsId, incidentsPostIncidentsIdSnoozeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.incident);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Incidents', 'postIncidentsIdSnooze', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const incidentsPostIncidentsIdStatusUpdatesBodyParam = {
      message: 'string'
    };
    describe('#postIncidentsIdStatusUpdates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postIncidentsIdStatusUpdates(incidentsId, incidentsPostIncidentsIdStatusUpdatesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.status_update);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Incidents', 'postIncidentsIdStatusUpdates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const incidentsPutIncidentsBodyParam = {
      incidents: [
        {}
      ]
    };
    describe('#putIncidents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putIncidents(incidentsPutIncidentsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Incidents', 'putIncidents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIncidents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIncidents(null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.incidents));
                assert.equal(1, data.response.limit);
                assert.equal(0, data.response.offset);
                assert.equal(true, data.response.more);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Incidents', 'getIncidents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const incidentsPutIncidentsIdBodyParam = {
      incident: {}
    };
    describe('#putIncidentsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putIncidentsId(incidentsId, incidentsPutIncidentsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Incidents', 'putIncidentsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIncidentsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIncidentsId(incidentsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.incident);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Incidents', 'getIncidentsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const incidentsPutIncidentsIdAlertsBodyParam = {
      alerts: [
        {
          type: 'alert',
          status: 'resolved',
          incident: {
            id: 'PEYSGVF',
            type: 'incident_reference'
          },
          body: {
            type: 'alert_body',
            contexts: [
              {
                type: 'link'
              }
            ],
            details: {
              customKey: 'Server is on fire!',
              customKey2: 'Other stuff!'
            }
          }
        }
      ]
    };
    describe('#putIncidentsIdAlerts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putIncidentsIdAlerts(incidentsId, incidentsPutIncidentsIdAlertsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Incidents', 'putIncidentsIdAlerts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIncidentsIdAlerts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIncidentsIdAlerts(incidentsId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.alerts));
                assert.equal(1, data.response.limit);
                assert.equal(0, data.response.offset);
                assert.equal(true, data.response.more);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Incidents', 'getIncidentsIdAlerts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const incidentsAlertId = 'fakedata';
    const incidentsPutIncidentsIdAlertsAlertIdBodyParam = {
      alert: {
        type: 'alert',
        status: 'resolved',
        incident: {
          id: 'PEYSGVF',
          type: 'incident_reference'
        },
        body: {
          type: 'alert_body',
          contexts: [
            {
              type: 'link'
            }
          ],
          details: {
            customKey: 'Server is on fire!',
            customKey2: 'Other stuff!'
          }
        }
      }
    };
    describe('#putIncidentsIdAlertsAlertId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putIncidentsIdAlertsAlertId(incidentsId, incidentsAlertId, incidentsPutIncidentsIdAlertsAlertIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Incidents', 'putIncidentsIdAlertsAlertId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIncidentsIdAlertsAlertId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIncidentsIdAlertsAlertId(incidentsId, incidentsAlertId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.alert);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Incidents', 'getIncidentsIdAlertsAlertId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIncidentsIdLogEntries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIncidentsIdLogEntries(incidentsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.log_entries));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Incidents', 'getIncidentsIdLogEntries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const incidentsPutIncidentsIdMergeBodyParam = {
      source_incidents: [
        {}
      ]
    };
    describe('#putIncidentsIdMerge - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putIncidentsIdMerge(incidentsId, incidentsPutIncidentsIdMergeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Incidents', 'putIncidentsIdMerge', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIncidentsIdNotes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIncidentsIdNotes(incidentsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.notes));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Incidents', 'getIncidentsIdNotes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPriorities - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPriorities((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.priorities));
                assert.equal(25, data.response.limit);
                assert.equal(0, data.response.offset);
                assert.equal(false, data.response.more);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Priorities', 'getPriorities', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const responsePlaysResponsePlayId = 'fakedata';
    const responsePlaysPostResponsePlaysResponsePlayIdRunBodyParam = {
      incident: {}
    };
    describe('#postResponsePlaysResponsePlayIdRun - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postResponsePlaysResponsePlayIdRun(responsePlaysResponsePlayId, responsePlaysPostResponsePlaysResponsePlayIdRunBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('ok', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResponsePlays', 'postResponsePlaysResponsePlayIdRun', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogEntries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLogEntries(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.log_entries));
                assert.equal(1, data.response.limit);
                assert.equal(0, data.response.offset);
                assert.equal(true, data.response.more);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LogEntries', 'getLogEntries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const logEntriesId = 'fakedata';
    describe('#getLogEntriesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLogEntriesId(null, logEntriesId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.log_entry);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LogEntries', 'getLogEntriesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const maintenanceWindowsPostMaintenanceWindowsBodyParam = {
      maintenance_window: {
        type: 'maintenance_window',
        start_time: '2015-11-09T20:00:00-05:00',
        end_time: '2015-11-09T22:00:00-05:00',
        description: 'Immanentizing the eschaton',
        services: [
          {
            id: 'PIJ90N7',
            type: 'service_reference'
          }
        ]
      }
    };
    describe('#postMaintenanceWindows - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMaintenanceWindows(maintenanceWindowsPostMaintenanceWindowsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.maintenanceWindow);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MaintenanceWindows', 'postMaintenanceWindows', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMaintenanceWindows - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMaintenanceWindows(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.maintenance_windows));
                assert.equal(25, data.response.limit);
                assert.equal(0, data.response.offset);
                assert.equal(false, data.response.more);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MaintenanceWindows', 'getMaintenanceWindows', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const maintenanceWindowsId = 'fakedata';
    const maintenanceWindowsPutMaintenanceWindowsIdBodyParam = {
      maintenance_window: {
        type: 'maintenance_window',
        start_time: '2015-11-09T20:00:00-05:00',
        end_time: '2015-11-09T22:00:00-05:00',
        description: 'Immanentizing the eschaton',
        services: [
          {
            id: 'PIJ90N7',
            type: 'service_reference'
          }
        ]
      }
    };
    describe('#putMaintenanceWindowsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putMaintenanceWindowsId(maintenanceWindowsPutMaintenanceWindowsIdBodyParam, maintenanceWindowsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MaintenanceWindows', 'putMaintenanceWindowsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMaintenanceWindowsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMaintenanceWindowsId(maintenanceWindowsId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.maintenanceWindow);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MaintenanceWindows', 'getMaintenanceWindowsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const notificationsSince = 'fakedata';
    const notificationsUntil = 'fakedata';
    describe('#getNotifications - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNotifications(null, notificationsSince, notificationsUntil, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.notifications));
                assert.equal(100, data.response.limit);
                assert.equal(0, data.response.offset);
                assert.equal(false, data.response.more);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Notifications', 'getNotifications', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOncalls - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOncalls(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.oncalls));
                assert.equal(25, data.response.limit);
                assert.equal(0, data.response.offset);
                assert.equal(false, data.response.more);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OnCalls', 'getOncalls', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const schedulesPostSchedulesBodyParam = {
      schedule: {
        name: 'Daily Engineering Rotation',
        type: 'schedule',
        time_zone: 'America/New_York',
        description: 'Rotation schedule for engineering',
        schedule_layers: [
          {
            name: 'Night Shift',
            start: '2015-11-06T20:00:00-05:00',
            end: '2016-11-06T20:00:00-05:00',
            rotation_virtual_start: '2015-11-06T20:00:00-05:00',
            rotation_turn_length_seconds: 86400,
            users: [
              {
                user: {
                  id: 'PXPGF42',
                  type: 'user'
                }
              }
            ],
            restrictions: [
              {
                type: 'daily_restriction',
                start_time_of_day: '08:00:00',
                duration_seconds: 32400
              }
            ]
          }
        ]
      }
    };
    describe('#postSchedules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSchedules(null, schedulesPostSchedulesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.schedule);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Schedules', 'postSchedules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const schedulesSince = 'fakedata';
    const schedulesUntil = 'fakedata';
    const schedulesPostSchedulesPreviewBodyParam = {
      schedule: {
        name: 'Daily Engineering Rotation',
        type: 'schedule',
        time_zone: 'America/New_York',
        description: 'Rotation schedule for engineering',
        schedule_layers: [
          {
            name: 'Night Shift',
            start: '2015-11-06T20:00:00-05:00',
            end: '2016-11-06T20:00:00-05:00',
            rotation_virtual_start: '2015-11-06T20:00:00-05:00',
            rotation_turn_length_seconds: 86400,
            users: [
              {
                user: {
                  id: 'PXPGF42',
                  type: 'user'
                }
              }
            ],
            restrictions: [
              {
                type: 'daily_restriction',
                start_time_of_day: '08:00:00',
                duration_seconds: 32400
              }
            ]
          }
        ]
      }
    };
    describe('#postSchedulesPreview - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSchedulesPreview(schedulesSince, schedulesUntil, null, schedulesPostSchedulesPreviewBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.schedule);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Schedules', 'postSchedulesPreview', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const schedulesId = 'fakedata';
    const schedulesPostSchedulesIdOverridesBodyParam = {
      override: {
        start: '2012-07-01T00:00:00-04:00',
        end: '2012-07-02T00:00:00-04:00',
        user: {
          id: 'PEYSGVF',
          type: 'user_reference'
        }
      }
    };
    describe('#postSchedulesIdOverrides - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSchedulesIdOverrides(schedulesId, schedulesPostSchedulesIdOverridesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.override);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Schedules', 'postSchedulesIdOverrides', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSchedules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSchedules(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.schedules));
                assert.equal(100, data.response.limit);
                assert.equal(0, data.response.offset);
                assert.equal(false, data.response.more);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Schedules', 'getSchedules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const schedulesPutSchedulesIdBodyParam = {
      schedule: {
        name: 'Daily Engineering Rotation',
        type: 'schedule',
        time_zone: 'America/New_York',
        description: 'Rotation schedule for engineering',
        schedule_layers: [
          {
            name: 'Night Shift',
            start: '2015-11-06T20:00:00-05:00',
            end: '2016-11-06T20:00:00-05:00',
            rotation_virtual_start: '2015-11-06T20:00:00-05:00',
            rotation_turn_length_seconds: 86400,
            users: [
              {
                user: {
                  id: 'PXPGF42',
                  type: 'user'
                }
              }
            ],
            restrictions: [
              {
                type: 'daily_restriction',
                start_time_of_day: '08:00:00',
                duration_seconds: 32400
              }
            ]
          }
        ]
      }
    };
    describe('#putSchedulesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putSchedulesId(schedulesId, null, schedulesPutSchedulesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Schedules', 'putSchedulesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSchedulesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSchedulesId(null, schedulesSince, schedulesUntil, schedulesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.schedule);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Schedules', 'getSchedulesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSchedulesIdOverrides - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSchedulesIdOverrides(schedulesId, schedulesSince, schedulesUntil, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.overrides));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Schedules', 'getSchedulesIdOverrides', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSchedulesIdUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSchedulesIdUsers(schedulesId, schedulesSince, schedulesUntil, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.users));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Schedules', 'getSchedulesIdUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const servicesPostServicesBodyParam = {
      service: {
        type: 'service',
        name: 'My Web App',
        description: 'My cool web application that does things.',
        auto_resolve_timeout: 14400,
        acknowledgement_timeout: 600,
        status: 'active',
        escalation_policy: {
          id: 'PWIP6CQ',
          type: 'escalation_policy_reference'
        },
        incident_urgency_rule: {
          type: 'use_support_hours',
          during_support_hours: {
            type: 'constant',
            urgency: 'high'
          },
          outside_support_hours: {
            type: 'constant',
            urgency: 'low'
          }
        },
        support_hours: {
          type: 'fixed_time_per_day',
          time_zone: 'America/Lima',
          start_time: '09:00:00',
          end_time: '17:00:00',
          days_of_week: [
            1,
            2,
            3,
            4,
            5
          ]
        },
        scheduled_actions: [
          {
            type: 'urgency_change',
            at: {
              type: 'named_time',
              name: 'support_hours_start'
            },
            to_urgency: 'high'
          }
        ],
        alert_creation: 'create_alerts_and_incidents',
        alert_grouping: 'time',
        alert_grouping_timeout: 2
      }
    };
    describe('#postServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postServices(servicesPostServicesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.service);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'postServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const servicesId = 'fakedata';
    const servicesPostServicesIdIntegrationsBodyParam = {
      integration: {}
    };
    describe('#postServicesIdIntegrations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postServicesIdIntegrations(servicesPostServicesIdIntegrationsBodyParam, servicesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.integration);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'postServicesIdIntegrations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServices(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.services));
                assert.equal(25, data.response.limit);
                assert.equal(0, data.response.offset);
                assert.equal(false, data.response.more);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'getServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const servicesPutServicesIdBodyParam = {
      service: {
        type: 'service',
        name: 'My Web App',
        description: 'My cool web application that does things.',
        auto_resolve_timeout: 14400,
        acknowledgement_timeout: 600,
        status: 'active',
        escalation_policy: {
          id: 'PWIP6CQ',
          type: 'escalation_policy_reference'
        },
        incident_urgency_rule: {
          type: 'use_support_hours',
          during_support_hours: {
            type: 'constant',
            urgency: 'high'
          },
          outside_support_hours: {
            type: 'constant',
            urgency: 'low'
          }
        },
        support_hours: {
          type: 'fixed_time_per_day',
          time_zone: 'America/Lima',
          start_time: '09:00:00',
          end_time: '17:00:00',
          days_of_week: [
            1,
            2,
            3,
            4,
            5
          ]
        },
        scheduled_actions: [
          {
            type: 'urgency_change',
            at: {
              type: 'named_time',
              name: 'support_hours_start'
            },
            to_urgency: 'high'
          }
        ],
        alert_creation: 'create_alerts_and_incidents',
        alert_grouping: 'time',
        alert_grouping_timeout: 2
      }
    };
    describe('#putServicesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putServicesId(servicesPutServicesIdBodyParam, servicesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'putServicesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServicesId(servicesId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.service);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'getServicesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const servicesIntegrationId = 'fakedata';
    const servicesPutServicesIdIntegrationsIntegrationIdBodyParam = {
      integration: {}
    };
    describe('#putServicesIdIntegrationsIntegrationId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putServicesIdIntegrationsIntegrationId(servicesPutServicesIdIntegrationsIntegrationIdBodyParam, servicesId, servicesIntegrationId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'putServicesIdIntegrationsIntegrationId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicesIdIntegrationsIntegrationId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServicesIdIntegrationsIntegrationId(servicesId, servicesIntegrationId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.integration);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'getServicesIdIntegrationsIntegrationId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const teamsPostTeamsBodyParam = {
      team: {
        type: 'team',
        name: 'Engineering',
        description: 'The engineering team'
      }
    };
    describe('#postTeams - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTeams(teamsPostTeamsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.team);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'postTeams', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeams - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeams(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.teams));
                assert.equal(100, data.response.limit);
                assert.equal(0, data.response.offset);
                assert.equal(false, data.response.more);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'getTeams', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const teamsId = 'fakedata';
    const teamsPutTeamsIdBodyParam = {
      team: {
        type: 'team',
        name: 'Engineering',
        description: 'The engineering team'
      }
    };
    describe('#putTeamsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putTeamsId(teamsPutTeamsIdBodyParam, teamsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'putTeamsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeamsId(teamsId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.team);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'getTeamsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const teamsEscalationPolicyId = 'fakedata';
    describe('#putTeamsIdEscalationPoliciesEscalationPolicyId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putTeamsIdEscalationPoliciesEscalationPolicyId(teamsId, teamsEscalationPolicyId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pagerduty-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'putTeamsIdEscalationPoliciesEscalationPolicyId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamsIdMembers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeamsIdMembers(teamsId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.members));
                assert.equal(100, data.response.limit);
                assert.equal(0, data.response.offset);
                assert.equal(false, data.response.more);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'getTeamsIdMembers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const teamsUserId = 'fakedata';
    const teamsPutTeamsIdUsersUserIdBodyParam = {
      role: 'observer'
    };
    describe('#putTeamsIdUsersUserId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putTeamsIdUsersUserId(teamsId, teamsUserId, teamsPutTeamsIdUsersUserIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pagerduty-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'putTeamsIdUsersUserId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersPostUsersBodyParam = {
      user: {
        type: 'user',
        name: 'Earline Greenholt',
        email: '125.greenholt.earline@graham.name',
        time_zone: 'America/Lima',
        color: 'green',
        role: 'admin',
        job_title: 'Director of Engineering',
        avatar_url: 'https://secure.gravatar.com/avatar/1d1a39d4635208d5664082a6c654a73f.png?d=mm&r=PG',
        description: 'I\'m the boss'
      }
    };
    describe('#postUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postUsers(usersPostUsersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'postUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersId = 'fakedata';
    const usersPostUsersIdContactMethodsBodyParam = {
      contact_method: {}
    };
    describe('#postUsersIdContactMethods - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postUsersIdContactMethods(usersId, usersPostUsersIdContactMethodsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.contactMethod);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'postUsersIdContactMethods', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersPostUsersIdNotificationRulesBodyParam = {
      notification_rule: {
        type: 'assignment_notification_rule',
        start_delay_in_minutes: 0,
        contact_method: {
          id: 'PXPGF42',
          type: 'email_contact_method'
        },
        urgency: 'high'
      }
    };
    describe('#postUsersIdNotificationRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postUsersIdNotificationRules(usersId, usersPostUsersIdNotificationRulesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.notificationRule);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'postUsersIdNotificationRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsers(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.users));
                assert.equal(25, data.response.limit);
                assert.equal(0, data.response.offset);
                assert.equal(false, data.response.more);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersMe - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsersMe(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsersMe', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersPutUsersIdBodyParam = {
      user: {
        type: 'user',
        name: 'Earline Greenholt',
        email: '125.greenholt.earline@graham.name',
        time_zone: 'America/Lima',
        color: 'green',
        role: 'admin',
        job_title: 'Director of Engineering',
        avatar_url: 'https://secure.gravatar.com/avatar/1d1a39d4635208d5664082a6c654a73f.png?d=mm&r=PG',
        description: 'I\'m the boss'
      }
    };
    describe('#putUsersId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putUsersId(usersPutUsersIdBodyParam, usersId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'putUsersId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsersId(usersId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsersId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersIdContactMethods - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsersIdContactMethods(usersId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.contact_methods));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsersIdContactMethods', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersContactMethodId = 'fakedata';
    const usersPutUsersIdContactMethodsContactMethodIdBodyParam = {
      contact_method: {}
    };
    describe('#putUsersIdContactMethodsContactMethodId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putUsersIdContactMethodsContactMethodId(usersId, usersContactMethodId, usersPutUsersIdContactMethodsContactMethodIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'putUsersIdContactMethodsContactMethodId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersIdContactMethodsContactMethodId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsersIdContactMethodsContactMethodId(usersId, usersContactMethodId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.contactMethod);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsersIdContactMethodsContactMethodId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersIdNotificationRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsersIdNotificationRules(usersId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.notification_rules));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsersIdNotificationRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersNotificationRuleId = 'fakedata';
    const usersPutUsersIdNotificationRulesNotificationRuleIdBodyParam = {
      notification_rule: {
        type: 'assignment_notification_rule',
        start_delay_in_minutes: 0,
        contact_method: {
          id: 'PXPGF42',
          type: 'email_contact_method'
        },
        urgency: 'high'
      }
    };
    describe('#putUsersIdNotificationRulesNotificationRuleId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putUsersIdNotificationRulesNotificationRuleId(usersId, usersNotificationRuleId, usersPutUsersIdNotificationRulesNotificationRuleIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'putUsersIdNotificationRulesNotificationRuleId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersIdNotificationRulesNotificationRuleId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsersIdNotificationRulesNotificationRuleId(usersId, usersNotificationRuleId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.notificationRule);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsersIdNotificationRulesNotificationRuleId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersIdSessions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsersIdSessions(usersId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.user_sessions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsersIdSessions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersType = 'fakedata';
    const usersSessionId = 'fakedata';
    describe('#getUsersIdSessionsTypeSessionId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsersIdSessionsTypeSessionId(usersId, usersType, usersSessionId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.user_session);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsersIdSessionsTypeSessionId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVendors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVendors((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.vendors));
                assert.equal(25, data.response.limit);
                assert.equal(0, data.response.offset);
                assert.equal(false, data.response.more);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendors', 'getVendors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vendorsId = 'fakedata';
    describe('#getVendorsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVendorsId(vendorsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.vendor);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendors', 'getVendorsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAddonsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAddonsId(addOnsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pagerduty-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AddOns', 'deleteAddonsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTagsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTagsId(contextualSearchId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pagerduty-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ContextualSearch', 'deleteTagsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEscalationPoliciesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteEscalationPoliciesId(escalationPoliciesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pagerduty-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EscalationPolicies', 'deleteEscalationPoliciesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtensionsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteExtensionsId(extensionsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pagerduty-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extensions', 'deleteExtensionsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMaintenanceWindowsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteMaintenanceWindowsId(maintenanceWindowsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pagerduty-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MaintenanceWindows', 'deleteMaintenanceWindowsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSchedulesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSchedulesId(schedulesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pagerduty-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Schedules', 'deleteSchedulesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const schedulesOverrideId = 'fakedata';
    describe('#deleteSchedulesIdOverridesOverrideId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSchedulesIdOverridesOverrideId(schedulesId, schedulesOverrideId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pagerduty-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Schedules', 'deleteSchedulesIdOverridesOverrideId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServicesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteServicesId(servicesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pagerduty-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'deleteServicesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTeamsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTeamsId(null, teamsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pagerduty-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'deleteTeamsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTeamsIdEscalationPoliciesEscalationPolicyId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTeamsIdEscalationPoliciesEscalationPolicyId(teamsId, teamsEscalationPolicyId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pagerduty-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'deleteTeamsIdEscalationPoliciesEscalationPolicyId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTeamsIdUsersUserId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTeamsIdUsersUserId(teamsId, teamsUserId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pagerduty-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'deleteTeamsIdUsersUserId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsersId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUsersId(usersId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pagerduty-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'deleteUsersId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsersIdContactMethodsContactMethodId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUsersIdContactMethodsContactMethodId(usersId, usersContactMethodId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pagerduty-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'deleteUsersIdContactMethodsContactMethodId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsersIdNotificationRulesNotificationRuleId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUsersIdNotificationRulesNotificationRuleId(usersId, usersNotificationRuleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pagerduty-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'deleteUsersIdNotificationRulesNotificationRuleId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsersIdSessions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUsersIdSessions(usersId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pagerduty-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'deleteUsersIdSessions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsersIdSessionsTypeSessionId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUsersIdSessionsTypeSessionId(usersId, usersType, usersSessionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pagerduty-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'deleteUsersIdSessionsTypeSessionId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
