
## 0.5.4 [10-15-2024]

* Changes made at 2024.10.14_21:14PM

See merge request itentialopensource/adapters/adapter-pagerduty!15

---

## 0.5.3 [09-14-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-pagerduty!13

---

## 0.5.2 [08-14-2024]

* Changes made at 2024.08.14_19:28PM

See merge request itentialopensource/adapters/adapter-pagerduty!12

---

## 0.5.1 [08-07-2024]

* Changes made at 2024.08.06_21:27PM

See merge request itentialopensource/adapters/adapter-pagerduty!11

---

## 0.5.0 [07-05-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/itsm-testing/adapter-pagerduty!10

---

## 0.4.0 [07-05-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/itsm-testing/adapter-pagerduty!10

---

## 0.3.4 [03-27-2024]

* Changes made at 2024.03.27_14:09PM

See merge request itentialopensource/adapters/itsm-testing/adapter-pagerduty!9

---

## 0.3.3 [03-13-2024]

* Changes made at 2024.03.13_15:08PM

See merge request itentialopensource/adapters/itsm-testing/adapter-pagerduty!8

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_14:49PM

See merge request itentialopensource/adapters/itsm-testing/adapter-pagerduty!7

---

## 0.3.1 [02-28-2024]

* Changes made at 2024.02.28_11:15AM

See merge request itentialopensource/adapters/itsm-testing/adapter-pagerduty!6

---

## 0.3.0 [12-31-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/itsm-testing/adapter-pagerduty!5

---

## 0.2.0 [05-25-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-pagerduty!4

---

## 0.1.4 [03-11-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/itsm-testing/adapter-pagerduty!3

---

## 0.1.3 [07-09-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-pagerduty!2

---

## 0.1.2 [01-14-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-pagerduty!1

---

## 0.1.1 [12-02-2019]

- Initial Commit

See commit 9b55145

---
