# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Pagerduty System. The API that was used to build the adapter for Pagerduty is usually available in the report directory of this adapter. The adapter utilizes the Pagerduty API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The PagerDuty adapter from Itential is used to integrate the Itential Automation Platform (IAP) with PagerDuty to support and enhance the IT service management processes by automating and streamlining incident response workflows, integrating with ITSM tools and providing analytics for service improvement. With this adapter you have the ability to perform operations such as:

- List, and Manage Incidents
- Get, Create, Update, or Delete a Maintenance Window
- Preview, Get, Create, Update, or Delete a Schedule

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
