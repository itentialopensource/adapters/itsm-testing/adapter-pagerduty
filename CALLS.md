## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for PagerDuty. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for PagerDuty.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the PagerDuty. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getApiReference(callback)</td>
    <td style="padding:15px">REST API Reference</td>
    <td style="padding:15px">{base_path}/{version}/api_reference?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAbilities(callback)</td>
    <td style="padding:15px">List abilities</td>
    <td style="padding:15px">{base_path}/{version}/abilities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAbilitiesId(id, callback)</td>
    <td style="padding:15px">Test an ability</td>
    <td style="padding:15px">{base_path}/{version}/abilities/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAddons(include = 'services', serviceIds, filter = 'full_page_addon', callback)</td>
    <td style="padding:15px">List installed add-ons</td>
    <td style="padding:15px">{base_path}/{version}/addons?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddons(addon, callback)</td>
    <td style="padding:15px">Install an add-on</td>
    <td style="padding:15px">{base_path}/{version}/addons?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAddonsId(id, callback)</td>
    <td style="padding:15px">Get an add-on</td>
    <td style="padding:15px">{base_path}/{version}/addons/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAddonsId(id, callback)</td>
    <td style="padding:15px">Delete an add-on</td>
    <td style="padding:15px">{base_path}/{version}/addons/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAddonsId(addon, id, callback)</td>
    <td style="padding:15px">Update an add-on</td>
    <td style="padding:15px">{base_path}/{version}/addons/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTags(query, callback)</td>
    <td style="padding:15px">List tags</td>
    <td style="padding:15px">{base_path}/{version}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTags(tag, callback)</td>
    <td style="padding:15px">Create a tag</td>
    <td style="padding:15px">{base_path}/{version}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTagsId(id, callback)</td>
    <td style="padding:15px">Get a tag</td>
    <td style="padding:15px">{base_path}/{version}/tags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTagsId(id, callback)</td>
    <td style="padding:15px">Delete a tag</td>
    <td style="padding:15px">{base_path}/{version}/tags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTagsIdEntityType(id, entityType = 'users', callback)</td>
    <td style="padding:15px">Get connected entities</td>
    <td style="padding:15px">{base_path}/{version}/tags/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEntityTypeIdTags(entityType = 'users', id, callback)</td>
    <td style="padding:15px">Get tags for entities</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postEntityTypeIdChangeTags(entityType = 'users', id, addRemove, callback)</td>
    <td style="padding:15px">Assign tags</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/change_tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEscalationPolicies(query, userIds, teamIds, include = 'services', sortBy = 'name', callback)</td>
    <td style="padding:15px">List escalation policies</td>
    <td style="padding:15px">{base_path}/{version}/escalation_policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postEscalationPolicies(escalationPolicy, callback)</td>
    <td style="padding:15px">Create an escalation policy</td>
    <td style="padding:15px">{base_path}/{version}/escalation_policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEscalationPoliciesId(id, include = 'services', callback)</td>
    <td style="padding:15px">Get an escalation policy</td>
    <td style="padding:15px">{base_path}/{version}/escalation_policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEscalationPoliciesId(id, callback)</td>
    <td style="padding:15px">Delete an escalation policy</td>
    <td style="padding:15px">{base_path}/{version}/escalation_policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putEscalationPoliciesId(escalationPolicy, id, callback)</td>
    <td style="padding:15px">Update an escalation policy</td>
    <td style="padding:15px">{base_path}/{version}/escalation_policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtensionSchemas(callback)</td>
    <td style="padding:15px">List extension schemas</td>
    <td style="padding:15px">{base_path}/{version}/extension_schemas?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtensionSchemasId(id, callback)</td>
    <td style="padding:15px">Get an extension vendor</td>
    <td style="padding:15px">{base_path}/{version}/extension_schemas/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtensions(extensionObjectId, query, extensionSchemaId, include = 'extension_objects', callback)</td>
    <td style="padding:15px">List extensions</td>
    <td style="padding:15px">{base_path}/{version}/extensions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExtensions(extension, callback)</td>
    <td style="padding:15px">Create an extension</td>
    <td style="padding:15px">{base_path}/{version}/extensions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtensionsId(id, include = 'extension_schemas', callback)</td>
    <td style="padding:15px">Get an extension</td>
    <td style="padding:15px">{base_path}/{version}/extensions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtensionsId(id, callback)</td>
    <td style="padding:15px">Delete an extension</td>
    <td style="padding:15px">{base_path}/{version}/extensions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtensionsId(extension, id, callback)</td>
    <td style="padding:15px">Update an extension</td>
    <td style="padding:15px">{base_path}/{version}/extensions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIncidents(since, until, dateRange = 'all', statuses = 'triggered', incidentKey, serviceIds, teamIds, userIds, urgencies = 'high', timeZone, sortBy, include = 'users', callback)</td>
    <td style="padding:15px">List incidents</td>
    <td style="padding:15px">{base_path}/{version}/incidents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIncidents(payload, callback)</td>
    <td style="padding:15px">Manage incidents</td>
    <td style="padding:15px">{base_path}/{version}/incidents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIncidents(incident, callback)</td>
    <td style="padding:15px">Create an Incident</td>
    <td style="padding:15px">{base_path}/{version}/incidents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIncidentsIdMerge(id, payload, callback)</td>
    <td style="padding:15px">Merge incidents</td>
    <td style="padding:15px">{base_path}/{version}/incidents/{pathv1}/merge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIncidentsId(id, callback)</td>
    <td style="padding:15px">Get an incident</td>
    <td style="padding:15px">{base_path}/{version}/incidents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIncidentsId(id, payload, callback)</td>
    <td style="padding:15px">Update an incident</td>
    <td style="padding:15px">{base_path}/{version}/incidents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIncidentsIdAlerts(id, statuses = 'triggered', alertKey, sortBy, include, callback)</td>
    <td style="padding:15px">List alerts for an incident</td>
    <td style="padding:15px">{base_path}/{version}/incidents/{pathv1}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIncidentsIdAlerts(id, alerts, callback)</td>
    <td style="padding:15px">Manage alerts</td>
    <td style="padding:15px">{base_path}/{version}/incidents/{pathv1}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIncidentsIdAlertsAlertId(id, alertId, callback)</td>
    <td style="padding:15px">Get an alert</td>
    <td style="padding:15px">{base_path}/{version}/incidents/{pathv1}/alerts/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIncidentsIdAlertsAlertId(id, alertId, alert, callback)</td>
    <td style="padding:15px">Update an alert</td>
    <td style="padding:15px">{base_path}/{version}/incidents/{pathv1}/alerts/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIncidentsIdLogEntries(id, timeZone, isOverview, include = 'incidents', callback)</td>
    <td style="padding:15px">List log entries for an incident</td>
    <td style="padding:15px">{base_path}/{version}/incidents/{pathv1}/log_entries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIncidentsIdNotes(id, callback)</td>
    <td style="padding:15px">List notes for an incident</td>
    <td style="padding:15px">{base_path}/{version}/incidents/{pathv1}/notes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIncidentsIdNotes(id, payload, callback)</td>
    <td style="padding:15px">Create a note on an incident</td>
    <td style="padding:15px">{base_path}/{version}/incidents/{pathv1}/notes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIncidentsIdStatusUpdates(id, payload, callback)</td>
    <td style="padding:15px">Create a status update on an incident</td>
    <td style="padding:15px">{base_path}/{version}/incidents/{pathv1}/status_updates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIncidentsIdResponderRequests(id, payload, callback)</td>
    <td style="padding:15px">Create a responder request for an incident</td>
    <td style="padding:15px">{base_path}/{version}/incidents/{pathv1}/responder_requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIncidentsIdSnooze(id, payload, callback)</td>
    <td style="padding:15px">Snooze an incident</td>
    <td style="padding:15px">{base_path}/{version}/incidents/{pathv1}/snooze?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPriorities(callback)</td>
    <td style="padding:15px">List priorities</td>
    <td style="padding:15px">{base_path}/{version}/priorities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResponsePlaysResponsePlayIdRun(responsePlayId, payload, callback)</td>
    <td style="padding:15px">Run a response play</td>
    <td style="padding:15px">{base_path}/{version}/response_plays/{pathv1}/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogEntries(timeZone, since, until, isOverview, include = 'incidents', callback)</td>
    <td style="padding:15px">List log entries</td>
    <td style="padding:15px">{base_path}/{version}/log_entries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogEntriesId(timeZone, id, include = 'incidents', callback)</td>
    <td style="padding:15px">Get a log entry</td>
    <td style="padding:15px">{base_path}/{version}/log_entries/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMaintenanceWindows(teamIds, serviceIds, include = 'teams', filter = 'past', query, callback)</td>
    <td style="padding:15px">List maintenance windows</td>
    <td style="padding:15px">{base_path}/{version}/maintenance_windows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMaintenanceWindows(maintenanceWindow, callback)</td>
    <td style="padding:15px">Create a maintenance window</td>
    <td style="padding:15px">{base_path}/{version}/maintenance_windows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMaintenanceWindowsId(id, include = 'teams', callback)</td>
    <td style="padding:15px">Get a maintenance window</td>
    <td style="padding:15px">{base_path}/{version}/maintenance_windows/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMaintenanceWindowsId(id, callback)</td>
    <td style="padding:15px">Delete or end a maintenance window</td>
    <td style="padding:15px">{base_path}/{version}/maintenance_windows/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putMaintenanceWindowsId(maintenanceWindow, id, callback)</td>
    <td style="padding:15px">Update a maintenance window</td>
    <td style="padding:15px">{base_path}/{version}/maintenance_windows/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNotifications(timeZone, since, until, filter = 'sms_notification', include = 'users', callback)</td>
    <td style="padding:15px">List notifications</td>
    <td style="padding:15px">{base_path}/{version}/notifications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOncalls(timeZone, include = 'escalation_policies', userIds, escalationPolicyIds, scheduleIds, since, until, earliest, callback)</td>
    <td style="padding:15px">List all of the on-calls</td>
    <td style="padding:15px">{base_path}/{version}/oncalls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchedules(query, callback)</td>
    <td style="padding:15px">List schedules</td>
    <td style="padding:15px">{base_path}/{version}/schedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSchedules(overflow, schedule, callback)</td>
    <td style="padding:15px">Create a schedule</td>
    <td style="padding:15px">{base_path}/{version}/schedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSchedulesPreview(since, until, overflow, schedule, callback)</td>
    <td style="padding:15px">Preview a schedule</td>
    <td style="padding:15px">{base_path}/{version}/schedules/preview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchedulesId(timeZone, since, until, id, callback)</td>
    <td style="padding:15px">Get a schedule</td>
    <td style="padding:15px">{base_path}/{version}/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSchedulesId(id, callback)</td>
    <td style="padding:15px">Delete a schedule</td>
    <td style="padding:15px">{base_path}/{version}/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSchedulesId(id, overflow, schedule, callback)</td>
    <td style="padding:15px">Update a schedule</td>
    <td style="padding:15px">{base_path}/{version}/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchedulesIdOverrides(id, since, until, editable, overflow, callback)</td>
    <td style="padding:15px">List overrides</td>
    <td style="padding:15px">{base_path}/{version}/schedules/{pathv1}/overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSchedulesIdOverrides(id, override, callback)</td>
    <td style="padding:15px">Create an override</td>
    <td style="padding:15px">{base_path}/{version}/schedules/{pathv1}/overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSchedulesIdOverridesOverrideId(id, overrideId, callback)</td>
    <td style="padding:15px">Delete an override</td>
    <td style="padding:15px">{base_path}/{version}/schedules/{pathv1}/overrides/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchedulesIdUsers(id, since, until, callback)</td>
    <td style="padding:15px">List users on call</td>
    <td style="padding:15px">{base_path}/{version}/schedules/{pathv1}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServices(teamIds, timeZone, sortBy = 'name', query, include = 'escalation_policies', callback)</td>
    <td style="padding:15px">List services</td>
    <td style="padding:15px">{base_path}/{version}/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postServices(service, callback)</td>
    <td style="padding:15px">Create a service</td>
    <td style="padding:15px">{base_path}/{version}/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicesId(id, include = 'escalation_policies', callback)</td>
    <td style="padding:15px">Get a service</td>
    <td style="padding:15px">{base_path}/{version}/services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServicesId(id, callback)</td>
    <td style="padding:15px">Delete a service</td>
    <td style="padding:15px">{base_path}/{version}/services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putServicesId(service, id, callback)</td>
    <td style="padding:15px">Update a service</td>
    <td style="padding:15px">{base_path}/{version}/services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postServicesIdIntegrations(integration, id, callback)</td>
    <td style="padding:15px">Create a new integration</td>
    <td style="padding:15px">{base_path}/{version}/services/{pathv1}/integrations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putServicesIdIntegrationsIntegrationId(integration, id, integrationId, callback)</td>
    <td style="padding:15px">Update an existing integration</td>
    <td style="padding:15px">{base_path}/{version}/services/{pathv1}/integrations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicesIdIntegrationsIntegrationId(id, integrationId, include = 'services', callback)</td>
    <td style="padding:15px">View an integration</td>
    <td style="padding:15px">{base_path}/{version}/services/{pathv1}/integrations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTeams(team, callback)</td>
    <td style="padding:15px">Create a team</td>
    <td style="padding:15px">{base_path}/{version}/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeams(query, callback)</td>
    <td style="padding:15px">List teams</td>
    <td style="padding:15px">{base_path}/{version}/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamsId(id, include = 'privileges', callback)</td>
    <td style="padding:15px">Get a team</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTeamsId(reassignmentTeam, id, callback)</td>
    <td style="padding:15px">Delete a team</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTeamsId(team, id, callback)</td>
    <td style="padding:15px">Update a team</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamsIdMembers(id, include = 'users', callback)</td>
    <td style="padding:15px">Get information about members on a team</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTeamsIdEscalationPoliciesEscalationPolicyId(id, escalationPolicyId, callback)</td>
    <td style="padding:15px">Remove an escalation policy from a team</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/escalation_policies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTeamsIdEscalationPoliciesEscalationPolicyId(id, escalationPolicyId, callback)</td>
    <td style="padding:15px">Add an escalation policy to a team</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/escalation_policies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTeamsIdUsersUserId(id, userId, callback)</td>
    <td style="padding:15px">Remove a user from a team</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/users/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTeamsIdUsersUserId(id, userId, role, callback)</td>
    <td style="padding:15px">Add a user to a team</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/users/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsers(query, teamIds, include = 'contact_methods', callback)</td>
    <td style="padding:15px">List users</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsers(user, callback)</td>
    <td style="padding:15px">Create a user</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersId(id, include = 'contact_methods', callback)</td>
    <td style="padding:15px">Get a user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUsersId(id, callback)</td>
    <td style="padding:15px">Delete a user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUsersId(user, id, callback)</td>
    <td style="padding:15px">Update a user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersMe(include = 'contact_methods', callback)</td>
    <td style="padding:15px">Get the current user</td>
    <td style="padding:15px">{base_path}/{version}/users/me?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersIdContactMethods(id, callback)</td>
    <td style="padding:15px">List a user's contact methods</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/contact_methods?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsersIdContactMethods(id, contactMethod, callback)</td>
    <td style="padding:15px">Create a user contact method</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/contact_methods?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersIdContactMethodsContactMethodId(id, contactMethodId, callback)</td>
    <td style="padding:15px">Get a user's contact method</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/contact_methods/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUsersIdContactMethodsContactMethodId(id, contactMethodId, callback)</td>
    <td style="padding:15px">Delete a user's contact method</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/contact_methods/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUsersIdContactMethodsContactMethodId(id, contactMethodId, user, callback)</td>
    <td style="padding:15px">Update a user's contact method</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/contact_methods/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersIdNotificationRules(id, include = 'contact_methods', callback)</td>
    <td style="padding:15px">List a user's notification rules</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/notification_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsersIdNotificationRules(id, notificationRule, callback)</td>
    <td style="padding:15px">Create a user notification rule</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/notification_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersIdNotificationRulesNotificationRuleId(id, notificationRuleId, include = 'contact_methods', callback)</td>
    <td style="padding:15px">Get a user's notification rule</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/notification_rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUsersIdNotificationRulesNotificationRuleId(id, notificationRuleId, callback)</td>
    <td style="padding:15px">Delete a user's notification rule</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/notification_rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUsersIdNotificationRulesNotificationRuleId(id, notificationRuleId, notificationRule, callback)</td>
    <td style="padding:15px">Update a user's notification rule</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/notification_rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersIdSessions(id, callback)</td>
    <td style="padding:15px">List a user's active sessions</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUsersIdSessions(id, callback)</td>
    <td style="padding:15px">Delete all user sessions</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersIdSessionsTypeSessionId(id, type, sessionId, callback)</td>
    <td style="padding:15px">Get a user's session</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/sessions/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUsersIdSessionsTypeSessionId(id, type, sessionId, callback)</td>
    <td style="padding:15px">Delete a user's session</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/sessions/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVendors(callback)</td>
    <td style="padding:15px">List vendors</td>
    <td style="padding:15px">{base_path}/{version}/vendors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVendorsId(id, callback)</td>
    <td style="padding:15px">Get a vendor</td>
    <td style="padding:15px">{base_path}/{version}/vendors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
