
## 0.2.0 [05-25-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-pagerduty!4

---

## 0.1.4 [03-11-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/itsm-testing/adapter-pagerduty!3

---

## 0.1.3 [07-09-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-pagerduty!2

---

## 0.1.2 [01-14-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-pagerduty!1

---

## 0.1.1 [12-02-2019]

- Initial Commit

See commit 9b55145

---
