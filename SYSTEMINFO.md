# PagerDuty

Vendor: PagerDuty
Homepage: https://www.pagerduty.com/

Product: PagerDuty
Product Page: https://www.pagerduty.com/

## Introduction
We classify PagerDuty into the ITSM (Service Management) domain as PagerDuty supports key ITSM functions, such as incident management, service operations and management.

## Why Integrate
The PagerDuty adapter from Itential is used to integrate the Itential Automation Platform (IAP) with PagerDuty to support and enhance the IT service management processes by automating and streamlining incident response workflows, integrating with ITSM tools and providing analytics for service improvement. With this adapter you have the ability to perform operations such as:

- List, and Manage Incidents
- Get, Create, Update, or Delete a Maintenance Window
- Preview, Get, Create, Update, or Delete a Schedule

## Additional Product Documentation
The [API documents for PagerDuty](https://developer.pagerduty.com/api-reference/e65c5833eeb07-pager-duty-api)